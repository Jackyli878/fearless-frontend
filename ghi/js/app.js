function createCard(name, description, pictureUrl, start_date, end_date, loation) {
  const startDate = new Date(start_date).toLocaleDateString('en-US', { month: 'numeric', day: 'numeric', year: 'numeric' });
  const endDate = new Date(end_date).toLocaleDateString('en-US', { month: 'numeric', day: 'numeric', year: 'numeric' });

  return `
    <div class="card shadow">
      <img src="${pictureUrl}" class="card-img-top" alt="Conference Image" placeholder="Conference Image Placeholder">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        ${startDate}-${endDate}
      </div>
    </div>
    <p></p>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Handle bad response
    } else {
      const data = await response.json();
      const columns = document.querySelectorAll('.col');
      let columnIndex = 0;

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start_date = details.conference.starts;
          const end_date = details.conference.ends;
          const location = details.conference.location.name;
          const html = createCard(name, description, pictureUrl, start_date, end_date, location);
          columns[columnIndex].innerHTML += html;

          columnIndex=(columnIndex+1) % columns.length
        }
      }
    }
  } catch (e) {
    // Handle error
  }
});
