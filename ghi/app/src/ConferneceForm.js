// import React, { useEffect, useState } from 'react';

// function NewConference() {
//   const [locations, setLocations] = useState([]);
//   const [name, setName] = useState("");
//   const [starts, setStarts] = useState("");
//   const [ends, setEnds] = useState("");
//   const [description, setDescription] = useState("");
//   const [maxPresentations, setMaxPresentations] = useState(0);
//   const [maxAttendees, setMaxAttendees] = useState(0);
//   const [selectedLocation, setSelectedLocation] = useState("");

//   const handleLocationChange = (event) => {
//     const value = event.target.value;
//     setSelectedLocation(value);
//   }

//   const handleNameChange = (event) => {
//     const value = event.target.value;
//     setName(value);
//   }

//   const handleStartsChange = (event) => {
//     const value = event.target.value;
//     setStarts(value);
//   }

//   const handleEndsChange = (event) => {
//     const value = event.target.value;
//     setEnds(value);
//   }

//   const handleDescriptionChange = (event) => {
//     const value = event.target.value;
//     setDescription(value);
//   }

//   const handleMaxPresentationsChange = (event) => {
//     const value = event.target.value;
//     setMaxPresentations(value);
//   }

//   const handleMaxAttendeesChange = (event) => {
//     const value = event.target.value;
//     setMaxAttendees(value);
//   }

//   const handleSubmit = async (event) => {
//     event.preventDefault();

//     const data = {};
//     data.name = name;
//     data.starts = starts;
//     data.ends = ends;
//     data.description = description;
//     data.max_presentations = maxPresentations;
//     data.max_attendees = maxAttendees;
//     data.location = selectedLocation;

//     const conferenceUrl = 'http://localhost:8000/api/conferences/';
//     const fetchConfig = {
//       method: "post",
//       body: JSON.stringify(data),
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     };

//     const response = await fetch(conferenceUrl, fetchConfig);
//     if (response.ok) {
//       const newConference = await response.json();
//       console.log(newConference);

//       setName('');
//       setStarts('');
//       setEnds('');
//       setDescription('');
//       setMaxPresentations('');
//       setMaxAttendees('');
//       setSelectedLocation('');
//     }
//   }

//   const fetchLocations = async () => {
//     const url = 'http://localhost:8000/api/locations/';

//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//       setLocations(data.locations);
//     }
//   }

//   useEffect(() => {
//     fetchLocations();
//   }, []);

//   return (
//     <div className="container">
//       <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Create a new Conference</h1>
//             <form onSubmit={handleSubmit}>
//               <div className="form-floating mb-3">
//                 <input placeholder="Name" required type="text" name="name" id="name" className="form-control" onChange={handleNameChange} value={name} />
//                 <label htmlFor="name">Name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" onChange={handleStartsChange} value={starts} />
//                 <label htmlFor="starts">Starts</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" onChange={handleEndsChange} value={ends} />
//                 <label htmlFor="ends">Ends</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <textarea placeholder="Description" required name="description" id="description" className="form-control" onChange={handleDescriptionChange} value={description}></textarea>
//                 <label htmlFor="description">Description</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" onChange={handleMaxPresentationsChange} value={maxPresentations} />
//                 <label htmlFor="max_presentations">Max Presentations</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" onChange={handleMaxAttendeesChange} value={maxAttendees} />
//                 <label htmlFor="max_attendees">Max Attendees</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <select onChange={handleLocationChange} required name="location" id="location" className="form-select" value={selectedLocation}>
//                   <option value="">Choose a location</option>
//                   {locations.map(location => (
//                     <option key={location.id} value={location.id}>
//                       {location.name}
//                     </option>
//                   ))}
//                 </select>
//                 <label htmlFor="location">Location</label>
//               </div>
//               <button className="btn btn-primary">Create</button>
//             </form>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// }

// export default NewConference;
